from flask import render_template
from app import app

@app.route('/mypage')
@app.route('/a')
def mypage():
    user = {'username': 'Andrea'}
    return render_template('mypage.html', title='Home',
                           user=user)